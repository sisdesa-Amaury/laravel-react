import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import API from '../librerias/api'
// import {browserHistory } from 'react-router';

// import MyGlobleSetting from './MyGlobleSetting';



export default class CrearProducto extends Component{
    constructor(props){
        super(props);
        this.state ={title:"",body:""}
    }

    handleChange1 = (e)=>{
        this.setState({
            title: e.target.value
          })
    }
    handleChange2 = (e)=>{
        this.setState({
            body: e.target.value
          })
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        const products = {
          title: this.state.title,
          body: this.state.body
        }
        let uri = '/api/products/';
        API.post(uri, products).then((response) => {
            // browserHistory.push('/display-item');
          
        });
    }
    
    render(){
        return (
            <form onSubmit={this.handleSubmit}>
            <h1>Create Post</h1>
            <div className="row">
                <div className="col-md-6">
                <div className="form-group">
                    <label>Post Title:</label>
                    <input type="text" className="form-control" onChange={this.handleChange1} />
                </div>
                </div>
                </div>
                <div className="row">
                <div className="col-md-6">
                    <div className="form-group">
                    <label>Post Content:</label>
                    <textarea className="form-control col-md-6" onChange={this.handleChange2}></textarea>
                    </div>
                </div>
                </div><br />
                <div className="form-group">
                <button className="btn btn-primary">Add Post</button>
            </div>
            </form>
        )
    }
}
