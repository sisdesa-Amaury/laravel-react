import React, { Component } from 'react';
import {
  withRouter, Link, Redirect
} from 'react-router-dom'
import API from '../librerias/api'
import  history   from '../librerias/history';

class TableRow extends Component {
  constructor(props) {
      super(props);
      this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit=(event) =>{
    event.preventDefault();
    history.push("/")
    // location.replace(API.defaults.baseURL )
    // API.delete(`/api/products/${this.props.obj.id}`);
    //   browserHistory.push('/display-item');
  }
  render() {
    return (
        <tr >
          <td>
            {this.props.obj.id}
          </td>
          <td>
           
            {this.props.obj.title}
          </td>
          <td>
            {this.props.obj.body}
          </td>
          <td>
          <form onSubmit={this.handleSubmit}>
            <Link to={"edit/"+this.props.obj.id} className="btn btn-primary">Edit</Link>
           <input type="submit" value="Delete" className="btn btn-danger"/>
         </form>
          </td>
        </tr>
    );
  }
}


export default TableRow;