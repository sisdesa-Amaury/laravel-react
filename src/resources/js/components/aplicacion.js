import React, { Component } from 'react';
import 'bulma/css/bulma.css'
import ReactDOM from 'react-dom';
import NavBar from './components/NavBar'

import { Router, Route  } from "react-router-dom";
import  history   from './librerias/history';

import CrearProducto from './pages/CrearProducto'
import DisplayProduct from './pages/DisplayProduct'
import UpdateProduct from './pages/UpdateProduct';

function Index() {
  return <h2>Home</h2>;
}

class AppRouter extends Component {



  render(){
    return (
      // <Router history={browserHistory}>
      //   <Route path="/" component={NavBar} >
      //     ejoe
      //   {/*  <Route path="/add-item" component={CrearProducto} />
      //     <Route path="/display-item" component={DisplayProduct} />
      //     <Route path="/edit/:id" component={UpdateProduct} /> */}
      //   </Route>
      // </Router>
      <Router history={history} >
         <NavBar>
          <Route path="/" exact component={Index} />
          <Route path="/creacion" component={CrearProducto} />
          <Route path="/display-item" component={DisplayProduct} />
          <Route path="/edit/:id" component={UpdateProduct} />
         </NavBar>
      </Router>
      );

  }

}

export default AppRouter;
if (document.getElementById('aplicacion')) {
    ReactDOM.render(<AppRouter />, document.getElementById('aplicacion'));
}

{/* <Router></Router> */}