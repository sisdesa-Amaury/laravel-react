<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    <style src="{{ asset('css/app.js') }}"></style>
    </head>
    <body>
    <div id="aplicacion"></div>
    </body>
    <script src="{{ asset('js/app.js') }}"></script>
</html>
